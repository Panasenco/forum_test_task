import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('AuthController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/auth/registration (POST)', () => {
    async function register(email, expectedStatus: number) {
      const response = await request(app.getHttpServer())
        .post('/auth/registration')
        .send({
          email: email,
          name: 'User name',
          password: 'password',
        })
        .expect(expectedStatus);

      return response.body;
    }

    it('should register new user', async () => {
      const token = await register('test@example.com1', 201);
      expect(token.access_token).toMatch(
        /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/,
      );
    });

    it('should not register user with exist email', async () => {
      await register('test@example.com2', 201);
      await register('test@example.com2', 400);
    });
  });

  afterEach(async () => {
    //todo: set up separate environments and clear database for test
  });

  afterAll(async () => {
    await app.close();
  });
});
