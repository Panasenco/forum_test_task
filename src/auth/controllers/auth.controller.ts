import { Controller, Post, UseGuards, Request, Body } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import LoginDto from '../dto/login.dto';
import {
  ApiBadRequestResponse,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { LocalAuthGuard } from '../strategies/local/local-auth.guard';
import CreateUserDto from '../dto/createUserDto';
import TokenDto from '../dto/token.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiUnauthorizedResponse({
    description: 'Invalid login or password.',
  })
  @ApiResponse({
    status: 201,
    description: 'Success login.',
    type: TokenDto,
  })
  async login(@Request() req, @Body() loginDto: LoginDto) {
    return this.authService.login(req.user);
  }

  @Post('registration')
  @ApiResponse({
    status: 201,
    description: 'Success registered.',
    type: TokenDto,
  })
  @ApiBadRequestResponse({ description: 'Provided invalid data' })
  async registration(@Body() createUserDto: CreateUserDto) {
    const token: TokenDto = await this.authService.registration(createUserDto);
    return token;
  }
}
