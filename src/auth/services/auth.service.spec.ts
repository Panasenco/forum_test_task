import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UsersService } from '../../users/services/users.service';
import * as bcrypt from 'bcrypt';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { UserDocument } from '../../users/schemas/user.schema';
import { UsersServiceMock } from '../../users/services/users.service.mock';

describe('AuthService', () => {
  let authService: AuthService;

  let mockUserService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: 'test',
          signOptions: { expiresIn: '60s' },
        }),
      ],
      providers: [
        {
          provide: 'USERS_SERVICE',
          useClass: UsersServiceMock,
        },
        AuthService,
        ConfigService,
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    mockUserService = module.get('USERS_SERVICE');
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  describe('login', () => {
    it('should login user with correct password', async function () {
      //prepare
      const testUser = {
        email: 'test@mail.wer',
        password: bcrypt.hashSync('pass', bcrypt.genSaltSync()),
      } as UserDocument;

      jest
        .spyOn(mockUserService, 'findOne')
        .mockImplementationOnce(() => Promise.resolve(testUser));

      //action
      const res = await authService.validateUser('test@mail.wer', 'pass');

      //testing
      expect(res).not.toBeNull();
      expect(res.email).toEqual(testUser.email);
    });

    it('should not login user with incorrect password', async function () {
      //prepare
      const testUser = {
        email: 'test@mail.wer',
        password: bcrypt.hashSync('pass', bcrypt.genSaltSync()),
      } as UserDocument;

      jest
        .spyOn(mockUserService, 'findOne')
        .mockImplementationOnce(() => Promise.resolve(testUser));

      //action
      const res = await authService.validateUser('test@mail.wer', 'pass2');

      //testing
      expect(res).toBeNull();
    });

    it('should not login user with incorrect login', async function () {
      //prepare
      jest
        .spyOn(mockUserService, 'findOne')
        .mockImplementationOnce(() => Promise.resolve(null));

      //action
      const res = await authService.validateUser('Yurii2', 'pass');

      //testing
      expect(res).toBeNull();
    });
  });
});
