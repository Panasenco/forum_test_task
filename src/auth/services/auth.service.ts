import { Inject, Injectable } from '@nestjs/common';
import { UsersService } from '../../users/services/users.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import CreateUserDto from '../dto/createUserDto';
import { ConfigService } from '@nestjs/config';
import { Environment } from '../../Environment.enum';
import TokenDto from '../dto/token.dto';
import { User, UserDocument } from '../../users/schemas/user.schema';

@Injectable()
export class AuthService {
  constructor(
    @Inject('USERS_SERVICE')
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async validateUser(
    email: string,
    password: string,
  ): Promise<Omit<User, 'password'>> {
    const user = await this.usersService.findOne(email);
    if (user && bcrypt.compareSync(password, user.password)) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: UserDocument): Promise<TokenDto> {
    const payload = { email: user.email, _id: user._id };
    return Promise.resolve({
      access_token: this.jwtService.sign(payload),
      refresh_token: 'not implemented yet', //todo: add reset_token
    });
  }

  async registration(createUserDto: CreateUserDto): Promise<TokenDto> {
    const salt = await bcrypt.genSalt(
      +this.configService.get(Environment.BCRYPT_SALT),
    );
    const user: User = Object.assign(new User(), createUserDto);
    user.password = await bcrypt.hash(user.password, salt);

    const res = await this.usersService.create(user);
    if (res) {
      return this.login(res);
    }
    throw new Error("Can't create user");
  }
}
