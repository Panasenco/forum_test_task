import { ApiProperty } from '@nestjs/swagger';

export default class TokenDto {
  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiIxIiwiaWF0IjoxNjI0MDMxODI2LCJleHAiOjE2MjQwNjc4MjZ9.OUEM0ziamF8NidhC6SPoUCxinFJkBTxOSBcdWO6-y33',
  })
  access_token: string;

  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiIxIiwiaWF0IjoxNjI0MDMxODI2LCJleHAiOjE2MjQwNjc4MjZ9.OUEM0ziamF8NidhC6SPoUCxinFJkBTxOSBcdWO6-y34',
  })
  refresh_token: string;
}
