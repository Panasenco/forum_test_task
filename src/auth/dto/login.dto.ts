import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export default class LoginDto {
  @ApiProperty({
    example: 'test@mail.com',
  })
  @IsNotEmpty()
  login: string;

  @ApiProperty({
    example: 'ddrdw324$2a_2',
  })
  @IsNotEmpty()
  password: string;
}
