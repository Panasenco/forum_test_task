import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export default class CreateUserDto {
  @ApiProperty({
    example: 'Yurii',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    example: 'test@mail.com',
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty({
    example: 'ddrdw324$2a_2',
  })
  @IsNotEmpty()
  @Length(5, 15)
  password: string;
}
