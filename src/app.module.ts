import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AuthService } from './auth/services/auth.service';
import { AuthModule } from './auth/auth.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Environment } from './Environment.enum';
import { MongooseModule } from '@nestjs/mongoose';
import { TopicModule } from './topic/topic.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    UsersModule,
    AuthModule,
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        return {
          uri: configService.get<string>(Environment.MONGODB_URI),
        };
      },
      inject: [ConfigService],
    }),
    TopicModule,
  ],
  providers: [AuthService],
})
export class AppModule {}
