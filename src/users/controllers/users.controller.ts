import { Controller, Get, Inject, Request, UseGuards } from '@nestjs/common';
import { UsersService } from '../services/users.service';
import { JwtAuthGuard } from '../../auth/strategies/jwt/jwt-auth.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('Users')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
  constructor(
    @Inject('USERS_SERVICE') private readonly userService: UsersService,
  ) {}

  @Get('profile')
  async info(@Request() req) {
    return req.user;
  }
}
