import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Role } from '../../auth/role.enum';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  email: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  password: string;

  @Prop({ required: true, default: Role.RegularUser })
  role: Role;

  @Prop()
  avatarPath: string;

  /**
   * when user login by password the response should contains pair of token
   * {access_token:string, refresh_token:string} - the refresh token will using
   * when the access_token will expire, each refresh_token can be used only one time
   */
  @Prop()
  refreshTokens: Array<string> = [];

  @Prop()
  createdAt: Date;

  @Prop()
  confirmedAt: Date;

  @Prop()
  deletedAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
