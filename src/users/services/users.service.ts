import { BadRequestException, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../schemas/user.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  public async findOne(email: string): Promise<UserDocument | undefined> {
    return this.userModel.findOne({ email: email }).lean();
  }

  public async findById(id: string): Promise<UserDocument | undefined> {
    return await this.userModel.findById(id).exec();
  }

  public async create(user: User): Promise<UserDocument> {
    const existUser = await this.findOne(user.email);
    if (existUser) {
      throw new BadRequestException( //todo: create own exception
        'User with email ' + user.email + ' is already exist',
      );
    }
    return await this.userModel.create(user);
  }
}
