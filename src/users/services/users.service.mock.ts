import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as bcrypt from 'bcrypt';
import { Environment } from '../../Environment.enum';
import { User } from '../schemas/user.schema';

@Injectable()
export class UsersServiceMock {
  private users: Array<User>;

  constructor(private readonly configService: ConfigService) {
    const sult = bcrypt.genSaltSync(
      +configService.get(Environment.BCRYPT_SALT),
    );
    this.initData(sult);
  }

  public async findOne(email: string): Promise<User | undefined> {
    return this.users.find((user) => user.email === email);
  }

  public create(user: User): Promise<boolean> {
    const existUser = this.users.find((u) => u.email == user.email);
    if (existUser) {
      throw new BadRequestException( //todo: create own exception
        'User with email ' + user.email + ' is already exist',
      );
    }
    this.users.push(user);
    return Promise.resolve(true);
  }

  private initData(bcryptSalt: string) {
    this.users = [
      {
        email: 'Yurii@er.qwe',
        password: bcrypt.hashSync('pass', bcryptSalt),
      },
      {
        email: 'Dima@er.qwe',
        password: bcrypt.hashSync('pass', bcryptSalt),
      },
    ] as Array<User>;
  }
}
