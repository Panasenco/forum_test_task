import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { User } from '../../users/schemas/user.schema';
import { TopicStatus } from './TopicStatus.enum';

export type TopicDocument = User & Topic;

export enum TopicActions {
  Manage = 'manage',
  Create = 'create',
  Read = 'read',
  Update = 'update',
  Delete = 'delete',
}

@Schema()
export class Topic {
  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  owner: User;

  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  content: string;

  @Prop({ required: true })
  status: TopicStatus = TopicStatus.Open;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }] })
  moderators: Array<User>;

  @Prop({ required: true })
  createdAt: Date = new Date();

  @Prop()
  updatedAt: Date;

  @Prop()
  deletedAt: Date;
}

export const TopicSchema = SchemaFactory.createForClass(Topic);
