import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export default class CreateTopicDto {
  @ApiProperty({
    example: 'Some interesting programming issue',
  })
  @IsNotEmpty()
  title: string;

  @ApiProperty({
    example: 'issue explanation links etc.',
  })
  @IsNotEmpty()
  content: string;
}
