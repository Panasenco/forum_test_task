import {
  Controller,
  Post,
  UseGuards,
  Request,
  Body,
  Get,
  Query,
  Delete,
} from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auth/strategies/jwt/jwt-auth.guard';
import CreateTopicDto from '../dto/create-topic.dto';
import { TopicService } from '../services/topic.service';
import SearchTopicDto from '../dto/search-topic.dto';
import AbilityGuard from '../../permission/ability.guard';

@ApiTags('Topic')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('topic')
export class TopicController {
  constructor(private readonly topicService: TopicService) {}

  @Get()
  @ApiQuery({ name: 'title', required: false })
  search(@Query() query: SearchTopicDto) {
    return this.topicService.search(query.title);
  }

  @Post()
  create(@Request() req, @Body() body: CreateTopicDto) {
    return this.topicService.create(body, req.user);
  }

  @UseGuards(AbilityGuard)
  @Delete(':id')
  async delete(@Request() req, @Query('id') id: string) {
    return this.topicService.delete(id, req.ability);
  }
}
