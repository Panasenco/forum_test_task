import { Test, TestingModule } from '@nestjs/testing';
import { TopicService } from './topic.service';
import { getModelToken } from '@nestjs/mongoose';
import { Topic } from '../schemas/Topic';

describe('TopicService', () => {
  let service: TopicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TopicService,
        {
          provide: getModelToken(Topic.name),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TopicService>(TopicService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  //todo: create a topic service test
});
