import {
  BadRequestException,
  Injectable,
  MethodNotAllowedException,
} from '@nestjs/common';
import CreateTopicDto from '../dto/create-topic.dto';
import { Model } from 'mongoose';
import { Topic, TopicActions, TopicDocument } from '../schemas/Topic';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../../users/schemas/user.schema';
import Ability from '../../permission/Ability';

@Injectable()
export class TopicService {
  constructor(
    @InjectModel(Topic.name) private topicModel: Model<TopicDocument>,
  ) {}

  async search(search: string) {
    let query = this.topicModel
      .where({ deletedAt: null })
      .populate('owner', ['name', 'email', 'avatarPath']);

    if (search) {
      query = query.where({
        title: { $regex: '.*' + search + '.*', $options: 'i' },
      });
    }

    return query.exec();
  }

  async create(createTopicDto: CreateTopicDto, author: User) {
    const newTopic = Object.assign(new Topic(), createTopicDto);
    newTopic.owner = author;
    newTopic.moderators = [author];
    return this.topicModel.create(newTopic);
  }

  async delete(id: string, ability: Ability) {
    const topic = await this.topicModel
      .findOne({ _id: id, deletedAt: null })
      .exec();

    if (!topic) {
      throw new BadRequestException("Can't file topic with Id " + id);
    }

    if (ability.can(TopicActions.Delete, Topic, topic)) {
      return this.topicModel.findByIdAndUpdate(id, {
        deletedAt: new Date(),
      });
    }
    throw new MethodNotAllowedException('Only owner can delete topic');
  }
}
