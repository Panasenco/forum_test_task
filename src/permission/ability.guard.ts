import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Topic, TopicActions } from '../topic/schemas/Topic';

type Rule = {
  action: string;
  entity: new () => any;
  validator?: (target: any) => boolean;
};

/**
 * this Guard can be used only if in request contains user object
 */
export default class AbilityGuard implements CanActivate {
  private rules: Array<Rule> = [];

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();

    this.rules.push({
      action: TopicActions.Delete,
      entity: Topic,
      validator: (target) =>
        request.user._id.toString() == target.owner.toString(),
    });

    request.ability = {
      can: this.checkPermission.bind(this),
    };
    return true;
  }

  private checkPermission(action, targetType, target): boolean {
    const entityRules = this.rules.filter((rule) => rule.entity == targetType);

    if (entityRules.length == 0) {
      return false;
    }

    const actionRules = entityRules.filter(
      (rule) => rule.action == action || rule.action == 'manage',
    );

    return actionRules.some((rule) => rule.validator(target));
  }
}
