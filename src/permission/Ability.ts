export default interface Ability {
  can(action: string, targetType: new () => any, target: any | undefined);
}
