To implements this project need to create this modules
- [ ] Auth - this module should be responsible for code that responsible for 
  Authorization and Authentication users, should provide endpoints for this functions
    - [x] registration
    - [x] login user by password
    - [ ] login user by OAuth2 (not clear what does it mean need clearify)
        * What does it mean? It is mean that user should have  an opportunity to 
          login by some services (in this case need list of allowed services), 
          or maybe it means that need to implement function that allow 
          to authenticate users in other services by using our forum, 
          or maybe it's means something other.
          Actually the OAuth2 is a protocol for Authorization but 
          not for Authentication, so need to clarify
    - [ ] *POST* - send reset password link
    - [ ] *POST* - reset password by temporary token  
    - [ ] confirm email by temporary token
- [ ] User - this module responsible for all manipulation with users and should provide 
  enpoints that allow do perform this actions 
    - [ ] *GET* specific user info should be available for admin
    - [x] *GET* user profile should be available for logined user
    - [ ] *PUT* user info should be available for logined user and for admin
    - [ ] *POST* user proto - is user post a photo the prevoius photo will be changed
    - [ ] *GET* list of users available only for Admin
    - [ ] *DELETE* user available only for Admin
        * What is need to do with users topics and posts?
    - [ ] 
- [ ] Topic - this is the main module of forum, is responsible for all action with topic 
  and should provide enpoints that allow do perform this actions
    - [x] *POST* - creates topic, allow for any logined user, user that create a topic is 
      moderator of it
    - [ ] *PUT* - updates a specific topic, allow for Moderators of topic or Admin
    - [ ] *GET* specific topic with list of comments, allow for any user
    - [x] *GET* all with list of comments - allow search topic by title is the title query 
      is empty the should be returned all topics
        * maybe need add pagination? 
    - [x] *DELETE* - a specific topic, allows only owner of the topic
    
    - [ ] mute user, allow for Moderators of topic or Admin, can have an period option
        - [ ] *POST* - mute user, if user muted without period all comments of this user 
          should be deleted from this topic
        - [ ] *GET* - list of muted users, this list should contains only users that have 
          been muted without period
        - [ ] *DELETE* - unmute user, allows only for users that have been muted without 
          period, deleted messages shouldn't be returned
        
    - [ ] comments
        - [ ] *POST* a new comment
        - [ ] *PUT* - update a specific comment, allow for owner of comment
        - [ ] *DELETE* - a specific comment, allow for owner of comment (should be visible for users) or by moderator
    - [ ] moderators
        - [ ] *POST* - add a new moderator
        - [ ] *DELETE* - delete moderator from topic
            * Can Admin delete owner of topic from the moderator list? 
- [ ] File - this is additional module that helps to manipulate with File system
  should allow this actions
    - [ ] save file 
    - [ ] delete file 
    - [ ] get file by link 
- [ ] Mail - this is additional module that helps working with Mailing system
  should allow this actions
    - [ ] send message to email



To implements this functions we use this data structure
```typescript
type UserSchema = {
  _id: ObjectId,
  name:string,
  email: string,                     // using as login
  password: string,                  //hash
  avatar_path?: string,

  role:"admin"|"regularUser",
  
  /**
   * when user login by password the response should contains pair of token
   * {access_token:string, refresh_token:string} - the refresh token will using
   * when the access_token will expire, each refresh_token can be used only one time
   */
  refresh_tokens:Array<string>,  
  
  created_at: Date, 
  confirmed_at?: Date,               //date & time of email confirmation
  deleted_at?: Date,
};

type TopicSchema = {
  _id: ObjectId,
  owner: ObjectId,                   //link to user collection
  title: string,
  content: string,
  status: "open"|"close",            //using Enum
  comments: Array<{
    _id: ObjectId,
    owner: ObjectId,                 //link to user collection
    content: string,
    
    deleteReason?: "owner"|"ban"|"moderate", // comments that was deleted by "ban"|"moderate" reasons will not show to user
    created_at: Date,
    updated_at?: Date,
    deleted_at?: Date,
  }>,
  banList: Array<{
    _id: ObjectId,
    user: ObjectId,                  //link to user collection
    created_at: Date,
    finish_at?: Date,                //date end of ban, if the finish_at(period) unset moderator can set it to now() to unban user
  }>
  moderators: Array<ObjectId>,       //links to user collection
  
  created_at: Date,
  updated_at?: Date,
  deleted_at?: Date,
};
```
