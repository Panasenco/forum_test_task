## Description

This project is forum that did as a simple test task requirements and based on NestJS framework. 

While this project in progress you can see the progress details in the [implementation plan](IMPLEMENTATION_STATUS.md) file.

## Installation

```bash
$ npm install

$ cp .evn.example .env
```
enter environment variables from to .env file

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

